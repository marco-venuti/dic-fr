# dic-fr
Mini dizionario di francese per il corso di Francese 1 in SNS.

I lemmi vanno inseriti nel file `dic.txt` nelle opportune sezioni.
Se il lemma è costituito da più parole, queste vanno separate con un underscore, mentre nella traduzione questo non è necessario.

Per creare il virtualenv e installare i pacchetti necessari eseguire nella `/` del repo:
```
$ virtualenv -p python3 venv
$ source venv/bin/activate
$ pip install -r requirements
```
Per compilare `dic.pdf` è sufficiente lanciare `dic.py`.

[Link al `.pdf` più recente](https://gitlab.com/marco-venuti/dic-fr/-/jobs/artifacts/master/raw/dic.pdf?job=compile_pdf).
