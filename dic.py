#!/usr/bin/env python3
import unidecode
import jinja2
import subprocess
import os
file = open('dic.txt')

# SOSTANTIVI
sost = []
for line in file:
    if line == '==SOSTANTIVI==\n':
        break

for line in file:
    if not line.startswith('#'):
        if line.startswith('=') or line.startswith('\n'):
            break
        sost.append(line[:len(line)-1].split(None, 2))
        if line.find('_') != -1:
            sost[len(sost)-1][0] = sost[len(sost)-1][0].replace('_', ' ')

# VERBI
file.seek(0, 0)
verbi = []
for line in file:
    if line == '==VERBI==\n':
        break

for line in file:
    if not line.startswith('#'):
        if line.startswith('=') or line.startswith('\n'):
            break
        verbi.append(line[:len(line)-1].split(None, 2))
        if line.find('_') != -1:
            verbi[len(verbi)-1][0] = verbi[len(verbi)-1][0].replace('_', ' ')

# AGGETTIVI
file.seek(0, 0)
agg = []
for line in file:
    if line == '==AGGETTIVI==\n':
        break

for line in file:
    if not line.startswith('#'):
        if line.startswith('=') or line.startswith('\n'):
            break
        agg.append(line[:len(line)-1].split(None, 1))
        if line.find('_') != -1:
            agg[len(agg)-1][0] = agg[len(agg)-1][0].replace('_', ' ')

# AVVERBI
file.seek(0, 0)
avv = []
for line in file:
    if line == '==AVVERBI==\n':
        break

for line in file:
    if not line.startswith('#'):
        if line.startswith('=') or line.startswith('\n'):
            break
        avv.append(line[:len(line)-1].split(None, 1))
        if line.find('_') != -1:
            avv[len(avv)-1][0] = avv[len(avv)-1][0].replace('_', ' ')

# PREPOSIZIONI
file.seek(0, 0)
prep = []
for line in file:
    if line == '==PREPOSIZIONI==\n':
        break

for line in file:
    if not line.startswith('#'):
        if line.startswith('=') or line.startswith('\n'):
            break
        prep.append(line[:len(line)-1].split(None, 1))
        if line.find('_') != -1:
            prep[len(prep)-1][0] = prep[len(prep)-1][0].replace('_', ' ')

file.close()
# CHECK FOR DUPLICATES

# ORDINA
sost.sort(key=lambda parola: unidecode.unidecode(parola[0].lower()))
verbi.sort(key=lambda parola: unidecode.unidecode(parola[0].lower()))
agg.sort(key=lambda parola: unidecode.unidecode(parola[0].lower()))
avv.sort(key=lambda parola: unidecode.unidecode(parola[0].lower()))
prep.sort(key=lambda parola: unidecode.unidecode(parola[0].lower()))

# JINJA2
file_loader = jinja2.FileSystemLoader('.')
env = jinja2.Environment(loader=file_loader)
render = env.get_template('template.tex').render(sost=sost, verbi=verbi, agg=agg, prep=prep)

# LATEX
p = subprocess.Popen(['pdflatex'], stdin=subprocess.PIPE, universal_newlines=True)
p.communicate(input=render)
